import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import User from "../../model/user";
import { signUp } from "../../services/auth.service";
import { GlobalStyle, Wrapper, Form, Input, Button, CardWrapper, CardBody, CardButton, CardFieldset, CardHeader, CardHeading, CardIcon, CardInput } from "../../syled-components/styled-components";


export const SignUp = ({error, setError}) => {
    const navigate = useNavigate();
    
    var user: User = {
        email: "",
        password: ""
    }
    const [datosUser, setdatosUser] = useState(user);
    
    const handleSubmit = async (e:any) => {
        e.preventDefault();
        signUp(datosUser.email,datosUser.password)
        //console.log(res.data)
        navigate("/login")
        
    };
    
   
    const handleChange = (evt:  React.ChangeEvent<HTMLInputElement>) => {
    setdatosUser({...datosUser,
        [evt.target.name]: evt.target.value});
    }
    
    return (
        <>

         <CardWrapper>
        <CardHeader>
          <CardHeading>Registrarse</CardHeading>
        </CardHeader>

        <CardBody>

          <CardFieldset>
            <CardInput 
                placeholder="E-mail" 
                type="text" 
                name="email"
                value={datosUser.email}
                onChange={handleChange} required />
          </CardFieldset>

          <CardFieldset>
            <CardInput 
                placeholder="Password" 
                type="password"
                name="password"
                value={datosUser.password}
                onChange={handleChange}
                required />
            <CardIcon className="fa fa-eye" />
          </CardFieldset>

          <CardFieldset>
            <CardButton type="button" onClick={handleSubmit}>Aceptar</CardButton>
          </CardFieldset>
        </CardBody>
      </CardWrapper>
         
        </>
    );
}