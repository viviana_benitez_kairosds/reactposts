import { useNavigate } from "react-router-dom";
import { Button } from "../../syled-components/styled-components";

export const Logout = () =>{
    const navigate = useNavigate();
  
    const handleLogout = ()=> {
          //borrar token del localstorage
          localStorage.removeItem('token')
          localStorage.removeItem('email')

          //borrar role del context? 

          //datos del usuario del context?

         //volver al inicio de sesion
         navigate("/");
    }

    return (
        <Button onClick={handleLogout}>Logout</Button>
    )
    
}