import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Role } from "../../model/role";
import { getTokenByAPI, getRole, getIdUser } from "../../services/auth.service";
import { GlobalStyle, Wrapper, Form, Input, Button, CardBody, CardButton, CardFieldset, CardHeader, CardHeading, CardIcon, CardInput, CardLink, CardOptions, CardOptionsItem, CardOptionsNote, CardWrapper } from "../../syled-components/styled-components";


export const Login = ({error,setError,dataUser,setdataUser}) => {
  
  
  const navigate = useNavigate();

  //const [dataUser, setdataUser] = useState(user);
  
      const handleSubmit = async (e:any) => {
        e.preventDefault();
        if(dataUser.email == '' || dataUser.password == ''){
          setError(true);
        }else{
          setError(false);
          const result = await getTokenByAPI(dataUser);
          localStorage.setItem('token',result.data.token);
          localStorage.setItem('email',dataUser.email);
          //obtener rol y guardarlo en un context para poder acceder a el a nivel global
          const rol = await getRole()
          const idUser = await getIdUser()
          setdataUser({...dataUser,role:rol.data, id: idUser.data})
          switch (rol.data) {
            case Role.ADMIN:
              navigate("/posts");
              break;

            case Role.AUTHOR:
              navigate("/postsAuthor");
              break;

            case Role.USER:
              navigate("/posts");
              break;
            
            default:
              break;
          }
        }
      };
    
   
    const handleChange = (evt:  React.ChangeEvent<HTMLInputElement>) => {
    setdataUser({...dataUser,
        [evt.target.name]: evt.target.value});
    }

    const handleSignUp = () => {
        navigate("signup")
    }
    
      return (
        <>
        <CardWrapper>
        <CardHeader>
          <CardHeading>Iniciar sesión</CardHeading>
        </CardHeader>

        <CardBody>

          <CardFieldset>
            <CardInput 
                placeholder="E-mail" 
                type="text" 
                name="email"
                value={dataUser.email}
                onChange={handleChange} required />
          </CardFieldset>

          <CardFieldset>
            <CardInput 
                placeholder="Password" 
                type="password"
                name="password"
                value={dataUser.password}
                onChange={handleChange}
                required />
            <CardIcon className="fa fa-eye" />
          </CardFieldset>

          <CardFieldset>
            <CardButton type="button" onClick={handleSubmit}>Iniciar</CardButton>
          </CardFieldset>

          <CardFieldset>
            <CardButton type="button" onClick={handleSignUp}>Registrarse</CardButton>
          </CardFieldset>
        </CardBody>
      </CardWrapper>
        </>
      );
}