import { useContext } from "react";
import { Role } from "../../model/role";
import { createPost, updatePostbyId } from "../../services/posts.service";
import { Form, Input, Button } from "../../syled-components/styled-components";
import { UserContext } from "../contexts/UserContext";


export const FormCreatePost = ({error, setError, posts, setPosts, post, setPost}) => {
    //const [posts, setPosts] = useState<PostModel[]>([]);
    //const [post, setPost] = useState<PostModel>({title: '',text:'',id:''});
    const {role,nickName} = useContext(UserContext);
    const fildsValidate = role===Role.ADMIN ? !post.title || !post.text || !post.nickName : !post.title || !post.text

    
    const handleSubmit = async (e:any) => {
        e.preventDefault();
        const nick = post.nickName ? post.nickName : localStorage.getItem('email')!;
    
            if( fildsValidate ){
              setError(true);

            }else{
              setError(false);
              if(post.id){
                const updatePost = await updatePostbyId(post.id,nick,post.title,post.text)
                const postsActualizados = posts.map((postChange)=> postChange.id === post.id ? updatePost : postChange)
                setPosts(postsActualizados)
                setPost({title: '', text:'', id:'', nickName: ''})
              }else{
                const newPost = (await createPost(post.title,post.text,nick)).data
                setPosts([...posts,newPost])
                setPost({title: '',text:'',id:'', nickName: ''})
              }
            }
            
         

        
        
    }

    const handleChange = (evt:  React.ChangeEvent<HTMLInputElement>) => {
        setPost({...post,
            [evt.target.name]: evt.target.value});
        
    }

    return (
      <>
        {error && '*Los campos no son válidos'}
          <Form  ref={()=>{}} onSubmit={handleSubmit}>
          <Input
            type="text"
            name="title"
            value={post.title}
            placeholder="Título"
            onChange={handleChange}
          />
          <Input
            type="text"
            name="text"
            value={post.text}
            placeholder="Texto"
            onChange={handleChange}
          />
          {
            role && role==Role.ADMIN ? 

            <Input
              type="text"
              name="nickName"
              value={post.nickName}
              placeholder="Autor"
              onChange={handleChange}
            />

            : ''
          }
          <Button>{ post.id ?  'Actualizar post' : 'Crear post'}</Button>
        </Form>
      </>
    )
}