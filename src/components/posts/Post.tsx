
import { useState } from "react";
import { PostOutput } from "../../model/post-output";
import { removePostById } from "../../services/posts.service";
import { Button, CardStats, CardStatWrapper, CardTextBody, CardTextDate, CardTextTitle, CardTextWrapper, CardWrapper, NavLink} from "../../syled-components/styled-components";



export const Post = ({ post, setPost,posts,setPosts }) => {

    console.log('contiene post, ',post);
    

    const [comments, setComments] = useState([])


    const eliminar = (id:string) => {
        //eliminar de bd
        removePostById(id)
        const postsUpdate= posts.filter(post => post.id!=id)
        setPosts(postsUpdate)
      }
  
      const seleccionar = (post: PostOutput) => {
        setPost(post);
      }

    return (
        <>
        <NavLink
            to={{
            pathname: "/comments" }}>
       
            <CardWrapper>
                <CardTextWrapper>
                    <CardTextTitle>{post.title}</CardTextTitle>
                    <CardTextDate>{post.nickName}</CardTextDate>
                    <CardTextBody>
                       {post.text}
                    </CardTextBody>
                </CardTextWrapper>
                <CardStatWrapper>
                    <CardStats>
                        <Button onClick={ () => { eliminar(post.id)} }>Eliminar</Button>
                    </CardStats>
                    <CardStats>
                        <Button onClick={ () => { seleccionar(post)} }>seleccionar</Button>
                    </CardStats>
                </CardStatWrapper>
            </CardWrapper>
            </NavLink>
        </>
    );
}