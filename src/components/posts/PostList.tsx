import { useContext, useEffect, useState } from "react";
import { FormCreatePost } from "./FormCreatePost";
import { Logout } from "../auth/Logout";
import { getAllPosts, removePostById } from "../../services/posts.service";
import { Button, CardContainer, CardStats, CardStatWrapper, CardTextBody, CardTextDate, CardTextTitle, CardTextWrapper, CardWrapper, LinkStyled, NavLink, Separator } from "../../syled-components/styled-components";
import { PostOutput } from "../../model/post-output";
import { UserContext } from "../contexts/UserContext";
import { Role } from "../../model/role";
import { Post } from "./Post";


export const PostList = ({ error, setError }) => {
  const [posts, setPosts] = useState<PostOutput[]>([]);

  const { role } = useContext(UserContext)

  const postOutput: PostOutput = {
    id: '',
    title: '',
    text: '',
    idUser: '',
    nickName: ''
  }

  const [post, setPost] = useState<PostOutput>(postOutput);

  const eliminar = (id: string) => {
    //eliminar de bd
    removePostById(id)
    const postsUpdate = posts.filter(post => post.id != id)
    setPosts(postsUpdate)
  }

  const seleccionar = (post: PostOutput) => {
    setPost(post);
  }

  //obtener posts iniciales los que viene de base de datos
  useEffect(() => {
    getAllPosts().then((result) => {
      setPosts(result);
    });
  }, [setPosts])

  return (
    <>
      {role == Role.ADMIN ?
        (
          <FormCreatePost error={error} setError={setError} posts={posts} setPosts={setPosts} post={post} setPost={setPost} />

        ) : ''

      }
      <Logout />
      <h1>Posts</h1>

      {
        posts && posts.map((post) => ( 
                      <>
                          <CardWrapper key={post.id}>
                              <CardTextWrapper>
                                  <CardTextTitle>{post.title}</CardTextTitle>
                                  <CardTextDate>{post.nickName}</CardTextDate>
                                  <CardTextBody>
                                    {post.text}
                                  </CardTextBody>
                              </CardTextWrapper>
                              <CardStatWrapper>
                                  <CardStats>
                                      <Button onClick={ () => { eliminar(post.id)} }>Eliminar</Button>
                                  </CardStats>
                                  <CardStats>
                                      <Button onClick={ () => { seleccionar(post)} }>seleccionar</Button>
                                  </CardStats>
                              </CardStatWrapper>
                            <NavLink
                            to={{
                            pathname: "/comments",
                            search: `id=${post.id}`}}>Detalles
                            </NavLink>
                          </CardWrapper>
                
                      </>
        ))
      }

    </>
  );
}