import { useContext, useEffect, useState } from "react";
import { FormCreatePost } from "./FormCreatePost";
import { Logout } from "../auth/Logout";
import { getAllPosts, removePostById } from "../../services/posts.service";
import { Button } from "../../syled-components/styled-components";
import { PostOutput } from "../../model/post-output";
import { Link } from "react-router-dom";
import { UserContext } from "../contexts/UserContext";
import { Role } from "../../model/role";

export const PostListAuthor = ({error,setError,dataUser}) => {
  const [posts, setPosts] = useState<PostOutput[]>([]);

  const postOutput : PostOutput = {
    id:'',
    title: '',
    text: '',
    idUser:'',
    nickName: ''
  }
  const [post, setPost] = useState<PostOutput>(postOutput);

  const eliminar = (id:string) => {
    //eliminar de bd
    removePostById(id)
    const postsUpdate= posts.filter(post => post.id!=id)
    setPosts(postsUpdate)
  }

  const seleccionar = (post: PostOutput) => {
    //console.log('seleccione',post)
    setPost(post);
  }

  const method = async () => {
    const posts = await getAllPosts()
    const postsFiltrados = posts.filter( post => post.nickName == localStorage.getItem('email'))
    setPosts(postsFiltrados)
  }

  //obtener posts iniciales los que viene de base de datos
  useEffect( () => {
    method()
  },[setPosts])
  

    return (
        <>
           <Logout />
            <FormCreatePost error={error} setError={setError} posts={posts} setPosts={setPosts} post={post} setPost={setPost}/>

            <h1>Posts de {dataUser.email}</h1>
            <table>
                <tbody>
                    <tr>
                        <th>Id</th>
                        <th>Título</th>
                        <th>Texto</th>
                        {/* <th>Acciones</th> */}
                    </tr>
              
                  {
                    posts && posts.map( (post) => ( 
                      <tr key={post.id}>
                          <td>{post.id}</td>
                          <td>{post.title}</td>
                          <td>{post.text}</td>
                          <td>
                            <Button onClick={ () => { eliminar(post.id)} }>eliminar</Button>
                            <Button onClick={ () => { seleccionar(post)} }>seleccionar</Button>
                              
                          <Link
                              to={{
                                pathname: "/comments",
                                search: `id=${post.id}`,
                                //hash: "#the-hash",
                                //state: { fromDashboard: true }
                              }}
                            >detalles</Link>
                          </td>
                      </tr>
                    ))
                  }
          
               </tbody>
            </table>
        </>
      );
}