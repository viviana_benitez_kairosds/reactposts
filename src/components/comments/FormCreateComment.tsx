import React, { useContext } from 'react'
import { Role } from '../../model/role';
import { createComment, updateCommentbyIdOfCommentAndPost } from '../../services/comments.service';
import { getAllOffensiveWords } from '../../services/offensive-words.service';
import { Form, Input, Button } from '../../syled-components/styled-components';
import { UserContext } from '../contexts/UserContext';

const FormCreateComment = ({idPost,comments, setComments, comment, setComment, error, setError}) => {

  const filterOffensiveWords = async(phrase: string) => {
      const offensiveWords = await getAllOffensiveWords();
      return offensiveWords.filter(word => phrase.includes(word.word))
  }

  
  
  const handleSubmit = async (e:any) => {
    e.preventDefault();
    
    if(!comment.content || (await filterOffensiveWords(comment.content)).length!=0){
      setError(true);
    }
    else{
      setError(false);
      if(comment.id){

        //editando comment pero tengo que pasarle el email desde el comment o token please viviana
        const updatecomment = await updateCommentbyIdOfCommentAndPost(idPost,comment.id, comment.nickName, comment.content)
        const commentsActualizados = comments.map((commentChange) => commentChange.id === comment.id ? updatecomment : commentChange)
        setComments(commentsActualizados)
        setComment({id: '',
            nickName:'',
            content: '',
            timestamp: ''})
      }else{
        //creando comment
        const newcomment = (await createComment(idPost, comment.nickName,comment.content)).data
        setComments([...comments,newcomment])
        setComment({id: '',
            nickName:'',
            content: '',
            timestamp: ''})
      }
    }
}

const handleChange = (evt:  React.ChangeEvent<HTMLInputElement>)=>  {
    setComment({...comment,[evt.target.name]:evt.target.value})    
}


return (
  <>
  {error && '*Los campos no son válidos, no debe estar vacío ni contener palabras ofensivas'}
    <Form  onSubmit={handleSubmit}>
    <Input
      type="text"
      name="content"
      value={comment.content}
      placeholder="Comentario"
      onChange={handleChange}
    />
    
    <Button>{ comment.id ?  'Actualizar comentario' : 'Crear comentario'}</Button>
  </Form>
  </>
)
}

export default FormCreateComment