import React, { useContext, useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import { fromEvent } from 'rxjs';
import CommentOutput from '../../model/comment-output';
import { Role } from '../../model/role';
import { getAllComments, removeCommentByIdOfCommentAndPost } from '../../services/comments.service';
import { Button } from '../../syled-components/styled-components';
import { Logout } from '../auth/Logout';
import { UserContext } from '../contexts/UserContext';
import FormCreateComment from './FormCreateComment';

const CommentsOfPostList = ({error, setError,setdataUser}) => {
  const {role,email} = useContext(UserContext)

  const [comments,setComments] = useState<CommentOutput[]>([]);
  const [comment,setComment] = useState<CommentOutput>({id: '',
                                                      nickName:'',
                                                      content: '',
                                                      timestamp: ''});
  const [searchParams, setSearchParams] = useSearchParams();
  const idPost = searchParams.get("id")
 
  
  console.log('idPost ',idPost);

  
  const eliminar = (idComentario: string) => {
    removeCommentByIdOfCommentAndPost(idPost!, idComentario)
      const commentsUpdate= comments.filter(comment => comment.id!=idComentario)
      setComments(commentsUpdate)
  }

  const seleccionar = (comment : CommentOutput) => {
    setComment(comment);
  }

  // fromEvent(window, 'beforeunload').subscribe(
  //   () => {
  //     console.log('save');
      
  //     // localStorage.getItem('role', role);
  //     // localStorage.getItem('token', token);
  //     // localStorage.getItem('email', email);

  //   }
  // )

  // console.log('load');
  
  const save = () => {
    const userContext = useContext(UserContext)
    localStorage.setItem('userContext',JSON.stringify(userContext));
  }
  window.addEventListener('beforeunload',save)
  

  //obtener comemnts de un id
  useEffect( () => {
    const userContext = localStorage.getItem('userContext')
    if(userContext){
      setdataUser(useContext)
      // localStorage.removeItem('userContext');
    }
    getAllComments(idPost!).then((comment) => {
      setComments(comment)
    })

  },[setComments])
   
  return (
    <>
       <Logout />
        <FormCreateComment idPost={idPost} comments={comments} setComments={setComments} comment={comment} 
        setComment={setComment} error={error} setError={setError}/>
        <h1>Comentarios</h1>
        { comments.length != 0 ?   
          <>
          <table>
            <tbody>
                <tr>
                    <th>Id comentario</th>
                    <th>Autor</th>
                    <th>Contenido</th>
                </tr>
                {/* <tr> */}
              {
                comments && comments.map( (comment) => ( 
                 
                    <tr key={comment.id}>
                        <td>{comment.id}</td>
                        <td>{comment.nickName}</td>
                        <td>{comment.content}</td>
                        {
                         role==Role.USER && email==comment.nickName  || role==Role.ADMIN ?
                          <td>
                            <Button onClick={ () => { eliminar(comment.id)} }>eliminar</Button>
                            <Button onClick={ () => { seleccionar(comment)} }>actualizar</Button>
                          </td>
                          : ''
                        }
                    </tr>
          
                ))
              }
            {/* </tr> */}
           </tbody>
          </table>
          </>
        :  (<h2>Este post aun no tiene comentarios</h2>)
        }
        
    </>
  );
}

export default CommentsOfPostList