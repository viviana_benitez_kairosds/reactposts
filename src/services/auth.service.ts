import axios from "axios";
import User from "../model/user";



const url : string = `http://localhost:3000/api/`;


export const getTokenByAPI = (user: User) => {
    // Send a POST request
    return axios({
        method: 'post',
        url: `${url}login`,
        data: {
            email: user.email,
            password: user.password
        }
    });
}


export const getRole = () => {
    return axios({
        method: 'get',
        url: `${url}auth/role/me`
    });
}


export const getIdUser = () => {
    return axios({
        method: 'get',
        url: `${url}auth/id/me`
    });
}

export const signUp = async (email: string, password: string) => {
    return await axios({
        method: 'post',
        url: `${url}sign-up`,
        data: {
            email: email,
            password: password
        }
    });
}
