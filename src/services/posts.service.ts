import axios from "axios";
import { PostOutput } from "../model/post-output";
import User from "../model/user";


const url : string = `http://localhost:3000/api/`;


export const createPost = async (title: string, text:string, nickName: string) => {
    // Send a POST request
    return await axios({
        method: 'post',
        url: `${url}posts`,
        data: {
            nickName: nickName,
            title: title,
            text: text
        }
    });
}

export const getPostById = async (idPost:string) => {
    return (await axios.get<Post[]>(`${url}post/${idPost}`)).data;
}


export const getAllPosts = async () => {
    return (await axios.get<PostOutput[]>(`${url}posts`)).data;
}


export const updatePostbyId = async (idPost:string, nickName:string, title:string, text:string) => {
    return (await axios.put<PostOutput>(`${url}post/${idPost}`, {nickName: nickName, title: title, text: text})).data;
}


export const removePostById = async (idPost:string) => {
    return (await axios.delete<void>(`${url}post/${idPost}`));
}


