import axios from "axios";
import { OffensiveWordOutput } from "../model/offensive-word-output";

const url : string = `http://localhost:3000/api/`;

export const getAllOffensiveWords = async () => {
    return (await axios.get<OffensiveWordOutput[]>(`${url}offensive-word/`)).data;
}