import axios from "axios";
import CommentOutput from "../model/comment-output";
import User from "../model/user";


const url : string = `http://localhost:3000/api/`;


export const createComment = async (idPost:string,email: string, comment:string) => {
    // Send a POST request
    return await axios({
        method: 'post',
        url: `${url}post/${idPost}/comments`,
        data: {
            email: email,
            comment: comment,
        }
    });
}

export const getCommentByIdOfCommentAndPost = async (idPost:string,idComment:string) => {
    return (await axios.get<CommentOutput>(`${url}post/${idPost}/comment/${idComment}`)).data;
}


export const getAllComments = async (idPost:string) => {
    return (await axios.get<CommentOutput[]>(`${url}post/${idPost}/comments`)).data;
}


export const updateCommentbyIdOfCommentAndPost = async (idPost:string,idComment:string, email:string, comment:string) => {
    return (await axios.put<CommentOutput[]>(`${url}post/${idPost}/comments/${idComment}`, {email: email, comment: comment})).data;
}

export const removeCommentByIdOfCommentAndPost = async (idPost:string,idComment:string) => {
    return (await axios.delete(`${url}post/${idPost}/comments/${idComment}`));
}


