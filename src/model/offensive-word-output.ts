export type OffensiveWordOutput = {
    id: string;
    level: number;
    word: string;
}