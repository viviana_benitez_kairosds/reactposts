export type PostOutput = {
    id: string;
    title:string;
    text:string;
    idUser: string;
    nickName: string;
}