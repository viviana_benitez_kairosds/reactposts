export default interface CommentOutput{
    id: string;
    nickName:string;
    content: string;
    timestamp: string;
}