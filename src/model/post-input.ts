export type PostInput = {
    title: string;
    text: string;
    nickName: string;
}