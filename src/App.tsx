import { useState } from 'react'
import './App.css'

import {BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import { Login } from './components/auth/Login'
import { SignUp } from './components/auth/Sign-up'
import { PostList } from './components/posts/PostList'
import CommentsOfPostList from './components/comments/CommentsOfPostList'
import { UserContext } from './components/contexts/UserContext'
import User from './model/user'
import { PostListAuthor } from './components/posts/PostListAuthor'
import { Logout } from './components/auth/Logout'


const ProtectedRoute = ({ children }) => {
  const token = localStorage.getItem('token')!
  if (!token) {
    return <Navigate to='/login'/>
  }

  return children;
}



function App() {

  var user: User = {
    email: "",
    password: "",
    role: "",
    id: ""
  }
  const [dataUser,setdataUser] = useState(user)
  const [error, setError] = useState(false);

  return (
    <div className="App">
        <BrowserRouter>
          <UserContext.Provider value={dataUser}>
            <Routes>
                <Route path='/login' element={<Login error={error} setError={setError} dataUser={dataUser} setdataUser={setdataUser} />}/>
                <Route path='/' element={<Login error={error} setError={setError} dataUser={dataUser} setdataUser={setdataUser} />}/>
                <Route path='/postsAuthor' element={<ProtectedRoute><PostListAuthor dataUser={dataUser} error={error} setError={setError}/></ProtectedRoute>}/>
                <Route path='/signup' element={<SignUp error={error} setError={setError}/>}/>
                <Route path='/comments' element={<ProtectedRoute><CommentsOfPostList  error={error} setError={setError} setdataUser={setdataUser}/></ProtectedRoute>}/>
                <Route path='/posts' element={<ProtectedRoute><PostList error={error} setError={setError}/></ProtectedRoute>}/>
                <Route path='*' element={<h1>Página no encontrada</h1> }/>
            </Routes>
          </UserContext.Provider>
        </BrowserRouter>
    </div>
  )
}

export default App
