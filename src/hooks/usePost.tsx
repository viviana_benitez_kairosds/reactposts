import React from 'react'


//hook que añade, elimina y actualiza
const usePost = () => {

    const addPost = () => {
        console.log('addPost');
    };

    const deletePost = () => {
        console.log('deletePost');
    };

    const updatePost = () => {
        console.log('updatePost');
    };
    
    return [addPost,deletePost,updatePost]
}

export default usePost