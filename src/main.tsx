import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'

import axios from 'axios';

axios.interceptors.request.use( (request:any) => {
    // add auth header with jwt if account is logged in and request is to the api url
    const token = localStorage.getItem('token');
    //const isApiUrl = request.url.includes('/login')

    if (token) {
        request.headers.common.Authorization = `Bearer ${token}`;
       
    }
    return request;
});


ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
)
